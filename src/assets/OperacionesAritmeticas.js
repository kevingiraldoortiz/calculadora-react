export function sumar(...numeros){
    let total = 0

    for(const numero in numeros){
        total += numero
    }

    return total
}

export function restar(...numeros){
    let total = 0

    for(const numero in numeros){
        total -= numero
    }

    return total
}

export function dividir(dividendo, divisor){
    return dividendo/divisor
}

export function multiplicar(...numeros)
{
    let total = 0

    for(const numero in numeros){
        total *= numero
    }

    return total
}

