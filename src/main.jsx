import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'
import Calculadora from './Components/Calculadora'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <Calculadora/>
  </React.StrictMode>,
)
