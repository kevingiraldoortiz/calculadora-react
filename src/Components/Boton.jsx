import './Boton.css'

const Boton = (params) => {
  const {texto, clase, funcion} = params
  return (
    <button className={clase} value={texto} onClick={funcion}>{texto}</button>
  )
}

export default Boton