import { useState } from 'react'
import Boton from './Boton'
import './calculadora.css'
import Switch from './Switch'

const Calculadora = () => {

  const [data, setData] = useState({ operacion: '', resultado: '' })

  const escritura = (event) => {
    const valor = event.target.value

    const esOperacion = valor === '+' || valor === '-' || valor === '*' || valor === '/' || valor === '%'

    if (data.operacion.length >= 10) return
    if (valor === '+/-' && data.operacion === '') return
    if (valor === '%' && data.operacion.includes('%')) return

    if (data.operacion.includes('Error')) {
      setData({...data, operacion: valor})
    } else if (data.resultado !== '' && data.operacion === '' && esOperacion) {
      setData({...data, operacion: `${data.resultado}` + valor})
    }else if (valor === '+/-' && data.operacion !== '') {
      
      if (data.operacion.slice(0,1) === '-') {
        setData({...data, operacion: `${data.operacion.slice(1, data.operacion.length)}`})
      } else {
        setData({...data, operacion: `-${data.operacion}`})
      }

    } else {
      setData({...data, operacion: `${data.operacion}` + valor})
    }    
  }

  const eliminar = () => {
    setData({...data, operacion: data.operacion.slice(0, data.operacion.length - 1)})
  }

  const limpiar = () => {
    setData({ operacion: '', resultado: '' })
  }

  const resultado = () => {
    try {
      let resultado = ''

      if (data.operacion.includes('%')) {
        const valores = data.operacion.split('%')
        resultado = eval(`${valores[1]}*(${valores[0]}/100)`)
      } else {
        resultado = eval(data.operacion)
      }

      setData({...data, resultado, operacion: ''})
    } catch (error) {
      setData({...data, operacion: 'Error'})
    }    
  }

  return (
    <main>
      <Switch />
      <div className='display'>
        <span className="resultado">{data.resultado}</span>
        <span className="display">{data.operacion}</span>
      </div>
      
      <div className='teclado'>
        <Boton texto='C' clase='gris' funcion={limpiar}/>
        <Boton texto='+/-' clase='gris' funcion={escritura}/>
        <Boton texto='%' clase='gris' funcion={escritura}/>
        <Boton texto='/' clase='operacion' funcion={escritura}/>
      </div>
      
      <div className='teclado'>
        <Boton texto='7' clase='numero' funcion={escritura}/>
        <Boton texto='8' clase='numero' funcion={escritura}/>
        <Boton texto='9' clase='numero' funcion={escritura}/>
        <Boton texto='*' clase='operacion' funcion={escritura}/>
      </div>
      
      <div className='teclado'>
        <Boton texto='4' clase='numero' funcion={escritura}/>
        <Boton texto='5' clase='numero' funcion={escritura}/>
        <Boton texto='6' clase='numero' funcion={escritura}/>
        <Boton texto='-' clase='operacion' funcion={escritura}/>
      </div>
      
      <div className='teclado'>
        <Boton texto='1' clase='numero' funcion={escritura}/>
        <Boton texto='2' clase='numero' funcion={escritura}/>
        <Boton texto='3' clase='numero' funcion={escritura}/>
        <Boton texto='+' clase='operacion' funcion={escritura}/>
      </div>
      
      <div className='teclado'>
        <Boton texto='.' clase='numero' funcion={escritura}/>
        <Boton texto='0' clase='numero' funcion={escritura}/>
        <Boton texto='◀' clase='numero' funcion={eliminar}/>
        <Boton texto='=' clase='operacion' funcion={resultado}/>
      </div>

    </main>
  )
}

export default Calculadora